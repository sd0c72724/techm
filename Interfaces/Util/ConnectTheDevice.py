import serial as ser
import serial.tools.list_ports as prtlst

import time

import serial
global COMs
COMs=[]
def getCOM():
    #global COMs
    pts= prtlst.comports()
    device_port=[]
    # for pt in pts:
    #     if 'USB' in pt[1]: #check 'USB' string in device description
    #         COMs.append(pt[0])
    #

    for pt in pts:
        #print pt
        if 'VID:PID=0403:6001 SER=A104JU5RA' in pt[2]:  # check 'USB' string in device description
            COMs.append(pt[0])
            #print "Found Port"
            #print pt[0]
            #print pt[2]
            print "Device found attached in :"+pt[0]
            device_port=pt[0]
            ser = serial.Serial(port=device_port, baudrate=115200, parity=serial.PARITY_NONE,
                                stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
    return ser

if("__main__"==__name__):
    getCOM()