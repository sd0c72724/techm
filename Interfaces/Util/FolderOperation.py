'''__author__ = ""
__copyright__ = "Copyright 2017, Honeywell Embedded System Testing"
__credits__ = []
__license__ = ""
__version__ = "1.0.1"
__maintainer__ = "Shakti Das"
__email__ = ""
__status__ = "Production"'''

import os
import os, os.path
from wxPython._wx import false
from os.path import dirname
import os
import errno

#Will create folder in the root Project d
def checkAndCreatefolder():
    check_to_create_folder=os.path.exists('../../LogFiles')

    if(check_to_create_folder!=True):#If file not exists then This block will be executed
        folder_path=(dirname(dirname(dirname(__file__))))
        folder_path=folder_path+"/LogFiles"
        os.mkdir(folder_path, 0755)
        os.mkdir(folder_path+"/Audio", 0755)
        os.mkdir(folder_path + "/Camera", 0755)
        os.mkdir(folder_path + "/DDR3", 0755)
        os.mkdir(folder_path + "/Display", 0755)
        os.mkdir(folder_path + "/eMMC", 0755)
        os.mkdir(folder_path + "/Ethernet", 0755)
        os.mkdir(folder_path + "/Gpio", 0755)
        os.mkdir(folder_path + "/I2C", 0755)
        os.mkdir(folder_path + "/PCIe", 0755)
        os.mkdir(folder_path + "/RTC", 0755)
        os.mkdir(folder_path + "/SD_MMC_Test", 0755)
        os.mkdir(folder_path + "/Uart", 0755)
        os.mkdir(folder_path + "/uboot", 0755)
        os.mkdir(folder_path + "/USB", 0755)
        os.mkdir(folder_path + "/Video", 0755)
        os.mkdir(folder_path + "/Watchdog", 0755)
        os.mkdir(folder_path + "/Wi-Fi", 0755)

        print "The Log Folder is created " + folder_path + "/LogFiles"
def crateLogFile(path):
    print ""

if("__main__"==__name__):
   checkAndCreatefolder()
   #print ""