#!/usr/bin/python
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def softwatchdogtest():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/softwatchdogtest.txt' , 'w') as f:
        ser.write('echo 0 > /dev/watchdog' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def disablesoftwatchdog():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/disablesoftwatchdog.txt' , 'w') as f:
        ser.write('echo V > /dev/watchdog' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close() 
