#!/usr/bin/python
import glob
import serial
import time


global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def fileSystem():
    with open('../../../UARTOUTPUT/filesystem.txt' , 'w') as f:
        ser.write('fdisk -l' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def availableSpace():
    with open('../../../UARTOUTPUT/availableSpace.txt' , 'w') as f:
        ser.write('df -h' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def createFile():
    with open('../../../UARTOUTPUT/createFile.txt' , 'w') as f:
        ser.write('echo Hello World > greeting' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def showContent():
    with open('../../LogFiles/eMMC/Log.txt' , 'w') as f:
        ser.write('cat greeting' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def listFiles():
    with open('../../LogFiles/eMMC/Log1.txt' , 'w') as f:
        ser.write('ls -la' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close() 
        ser.close()
