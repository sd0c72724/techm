#!/usr/bin/python
import glob
import serial
import time
from Interfaces.Util import ConnectTheDevice

global ser

#ser = serial.Serial( port='COM3',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)
ser=ConnectTheDevice.getCOM()#Function will return the serial port along with connection Object
def captureImage():
    with open('../../LogFiles/Camera/Camera.txt' , 'w') as f:
        ser.write('./mx6s_v4l2_output.sh' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()


if("__main__"==__name__):
    captureImage()