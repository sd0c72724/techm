#!/usr/bin/python
import os
import sys
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def setupGpio():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setupGpio.txt' , 'w') as f:
        ser.write('ls /sys/class/gpio' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def systemWrite():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/systemWrite.txt' , 'w') as f:
        ser.write('echo 147 > /sys/class/gpio/export' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def setpinInput():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setpinInput.txt' , 'w') as f:
        ser.write('echo out > /sys/class/gpio/gpio147/direction' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def setpinOutput():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setpinOutput.txt' , 'w') as f:
        ser.write('echo in > /sys/class/gpio/gpio147/direction' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def setpinHigh():
    with open('../../LogFiles/Gpiot/ethernetCableStatus.txt' , 'w') as f:
        ser.write('echo 1 > /sys/class/gpio/gpio147/value' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def setpinLow():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setpinLow.txt' , 'w') as f:
        ser.write('echo 0 > /sys/class/gpio/gpio147/value' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
                
def currentValue():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/currentValue.txt' , 'w') as f:
        ser.write('cat /sys/class/gpio/gpio147/value' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()        
        ser.close()
