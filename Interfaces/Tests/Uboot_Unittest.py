#!/usr/bin/python
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)
ser.isOpen()


def uboottest():
    list1 = ['root','reboot','version','bdinfo','mmc info','mmc part','mmc dev 0','boot'];
    buffer1 = ''

    for i,val in enumerate(list1):
        with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/result%i.txt' %i, 'w') as f:
            if val == 'root':
                ser.write(val + '\r\n')
                time.sleep(3)
            
            if val == 'reboot':
                ser.write(val + '\r\n')
                while True:
                    buffer1 += ser.read(ser.inWaiting())
                    print buffer1
                    if '\n' in buffer1:
                        if 'Hit any key to stop autoboot:' in buffer1:
                            ser.write('\r\n')
                            #print "Executing"                           
                            break
            else:
                ser.write(val + '\r\n')
                time.sleep(5)
                out = ser.read(ser.inWaiting())
                f.writelines(out)
                f.close()
    ser.close()
            
#uboottest()
