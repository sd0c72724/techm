#!/usr/bin/python
import os
import sys
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def detectI2cDevices():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/detectI2cDevices.txt' , 'w') as f:
        ser.write('ls /dev/i2c*' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def scanDevices():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/scanDevices.txt' , 'w') as f:
        ser.write('i2cdetect -y 0' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()
