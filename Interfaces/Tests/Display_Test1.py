#!/usr/bin/python
import glob
import serial
import time


global ser
ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)


def Display_FrameBuffer():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/Display_FrameBuffer.txt' , 'w') as f:
        ser.write('ls /dev/fb* ' + '\r\n')
        time.sleep(1)
        readout = ser.read(ser.inWaiting())
        out = readout[21:29]
        f.writelines(out)
        f.close()

def Display_Resolution_Bit_Depth():
    with open('../../LogFiles/Display/Log.txt' , 'w') as f:
        ser.write('fbset' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        f.close()    

def Display_Send_Random_data():
    with open('../../LogFiles/Display/Log1.txt' , 'w') as f:
        ser.write('dd if=/dev/urandom of=/dev/fb0 bs=7000 count=600' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        f.close()

def Display_Clear():
    with open('../../LogFiles/Display/Log3.txt' , 'w') as f:
        ser.write('dd if=/dev/zero of=/dev/fb0 bs=7000 count=600' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        f.close()
        ser.close()
    

    
