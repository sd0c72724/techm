#!/usr/bin/python
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def listUsbDevices():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/listUsbDevices.txt' , 'w') as f:
        ser.write('lsusb' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def mountUsbDevice():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/mountUsbDevice.txt' , 'w') as f:
        ser.write('mount -t vfat /dev/sdd1 /mnt' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close() 
