#!/usr/bin/python
import os
import sys
import glob
import serial
import time
import shutil

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def detectUartDevice():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/detectUartDevice.txt' , 'w') as f:
        ser.write('dmesg | grep tty' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def setSerial():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setSerial.txt' , 'w') as f:
        ser.write('setserial -g /dev/ttyS[0123]' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def availableUart():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/availableUart.txt' , 'w') as f:
        ser.write('ls /dev/ttymxc' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def setBaudrate():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setBaudrate.txt' , 'w') as f:
        ser.write('stty -F /dev/ttymxc1 115200 raw -echo' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def listenSend():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/listenSend.txt' , 'w') as f:
        ser.write('# cat /dev/ttymxc1 & # echo Hello World > /dev/ttymxc2 Hello World' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close() 

def stopBackgroundProcess():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/stopBackgroundProcess.txt' , 'w') as f:
        ser.write('fg' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()         
