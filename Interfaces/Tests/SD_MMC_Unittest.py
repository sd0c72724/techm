#!/usr/bin/python
import os
import sys
import glob
import serial
import time
import shutil

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def availablespace():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/availablespace.txt' , 'w') as f:
        ser.write('df -h' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def ContentofFile():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/ContentofFile.txt' , 'w') as f:
        ser.write('ls /mnt/sdcard/' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()        
