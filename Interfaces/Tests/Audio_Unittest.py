#!/usr/bin/python
import time

import serial

from Interfaces.Util import ConnectTheDevice

ser=ConnectTheDevice.getCOM()#Function will return the serial port along with connection Object



def playAudioFile():


    with open('../../LogFiles/Audio/Audio.txt', 'w') as f:
        ser.write('./unit_test/audio_test_unit/test_stereo_44100Hz_16bit_PCM.wav' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        print out
        f.writelines(out)
        time.sleep(1)
        f.close()
        ser.close()
if("__main__"==__name__):
    playAudioFile()