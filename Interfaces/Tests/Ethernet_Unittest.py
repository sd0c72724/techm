#!/usr/bin/python
import os
import sys
import glob
import serial
import time
import ipaddress

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def ethernetCableInfo():
    with open('../../LogFiles/Ethernet/ethernetCableInfo.txt' , 'w') as f:
        ser.write('ifconfig' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        
def ethernetCableStatus():
    with open('../../LogFiles/Ethernet/ethernetCableStatus.txt' , 'w') as f:
        ser.write('ifconfig ethx up/down' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def ethernetRouting():
    with open('../../LogFiles/Ethernet/ethernetRouting.txt' , 'w') as f:
        ser.write('route -n' + '\r\n')
        time.sleep(3)
        out = ser.read(ser.inWaiting())
        #print out
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()

def gwDefaultAddress():
    with open('../../LogFiles/Ethernet/gwDefaultAddress.txt' , 'w') as f:
        ser.write('route add default gw 10.53.162.1 eth0' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()    

def pingIpAddress():
    with open('../../LogFiles/Ethernet/pingIpAddress.txt' , 'w') as f:
        ser.write(' Ping 10.53.162.133' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        #print out
        f.writelines(out)
        time.sleep(5)
        f.close()

def configureDhcp():
    with open('../../LogFiles/Ethernet/configureDhcp.txt' , 'w') as f:
        ser.write(' udhcp -i eth0' + '\r\n')
        ser.write(' udhcp -i eth1' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        #print out
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close()
