*** Settings ***
Library           Wi-Fi_Unittest.py

*** Test Cases ***
WiFi_Device_Information
    listwifidevices

WiFi_Device_Status
    wifidevstatus

WiFi_Device_Link_Status
    wifidevconnect

WiFi_Device_IP_Info
    wifishowip

WiFi_Device_Scan
    wifidevscan
