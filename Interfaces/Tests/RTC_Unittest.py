#!/usr/bin/python
import glob
import serial
import time

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def setTIme():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/setTIme.txt' , 'w') as f:
        ser.write('date -s 2017.09.15-10:10:10' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def CheckTime():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/CheckTime.txt' , 'w') as f:
        ser.write('date' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def networkTime():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/networkTime.txt' , 'w') as f:
        ser.write('ntpd -q -p us.pool.ntp.org' + '\r\n')
        ser.write('hwclock -r' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()

def hardwareclock():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/hardwareclock.txt' , 'w') as f:
        ser.write('hwclock -w' + '\r\n')
        time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        time.sleep(5)
        f.close()
        ser.close() 
