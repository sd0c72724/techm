*** Settings ***
Library           Ethernet_Unittest.py

*** Test Cases ***
ethernetCableInfo
    ethernetCableInfo

ethernetCableStatus
    ethernetCableStatus

ethernetRouting
    ethernetRouting

gwDefaultAddress
    gwDefaultAddress

pingIpAddress
    pingIpAddress

ConfigureDhcp
    ConfigureDhcp
