#!/usr/bin/python
import glob
import serial
import time


#import logging
#from robot.output import librarylogger
#from robot.running.context import EXECUTION_CONTEXTS

global ser

ser = serial.Serial( port='COM6',baudrate=115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)

def listwifidevices():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/WiFi_listdevices.html' , 'w') as f:
        ser.write('iw dev' + '\r\n')
        ser.flush()
        #time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        #time.sleep(5)
        
       # print "*HTML* AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        #write('THIS IS EXAMPLE', 'ERROR', html=False)
        #print "*HTML* The test file is found at <a href=WiFi_listdevices.html>this location</a>"
        f.close()
        
def wifidevstatus():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/WiFi_devicesstatus.txt' , 'w') as f:
        ser.write('iplink show wlan0' + '\r\n')
        ser.flush()
        #time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        #time.sleep(5)
        f.close()

def wifidevconnect():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/WiFi_devicesconnected.txt' , 'w') as f:
        ser.write('iw wlan0 link' + '\r\n')
        ser.flush()
        #time.sleep(1)
        out = ser.read(ser.inWaiting())
        f.writelines(out)
        #time.sleep(5)
        f.close()    

def wifishowip():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/WiFi_showip.txt' , 'w') as f:
        ser.write(' ip addr show wlan0' + '\r\n')
        ser.flush()
        #time.sleep(1)
        out = ser.read(ser.inWaiting())
        #print out
        f.writelines(out)
        #time.sleep(5)
        f.close()
        
def wifidevscan():
    with open('C:/Users/dj00427975/Desktop/UARTOUTPUT/WiFi_devicescan.txt' , 'w') as f:
        ser.write('iw dev wlan0 scan ap-force' + '\r\n')
        ser.flush()
        #time.sleep(3)
        out = ser.read(ser.inWaiting())
        #print out
        f.writelines(out)
        #time.sleep(5)
        f.close()
        ser.close()

'''    
def write(msg, level='INFO', html=False):
    """Writes the message to the log file using the given level.

    Valid log levels are ``TRACE``, ``DEBUG``, ``INFO`` (default since RF
    2.9.1), ``WARN``, and ``ERROR`` (new in RF 2.9). Additionally it is
    possible to use ``HTML`` pseudo log level that logs the message as HTML
    using the ``INFO`` level.

    Instead of using this method, it is generally better to use the level
    specific methods such as ``info`` and ``debug`` that have separate
    ``html`` argument to control the message format.
    """
    if EXECUTION_CONTEXTS.current is not None:
        librarylogger.write(msg, level, html)
    else:
        logger = logging.getLogger("RobotFramework")
        level = {'TRACE': logging.DEBUG/2,
                 'DEBUG': logging.DEBUG,
                 'INFO': logging.INFO,
                 'HTML': logging.INFO,
                 'WARN': logging.WARN,
                 'ERROR': logging.ERROR}[level]
        logger.log(level, msg)


def trace(msg, html=False):
    """Writes the message to the log file using the ``TRACE`` level."""
    write(msg, 'TRACE', html)

def debug(msg, html=False):
    """Writes the message to the log file using the ``DEBUG`` level."""
    write(msg, 'DEBUG', html)

def info(msg, html=False, also_console=False):
    """Writes the message to the log file using the ``INFO`` level.

    If ``also_console`` argument is set to ``True``, the message is
    written both to the log file and to the console.
    """
    write(msg, 'INFO', html)
    if also_console:
        console(msg)

def warn(msg, html=False):
    """Writes the message to the log file using the ``WARN`` level."""
    write(msg, 'WARN', html)

def error(msg, html=False):
    """Writes the message to the log file using the ``ERROR`` level.

    New in Robot Framework 2.9.
    """
    write(msg, 'ERROR', html)

def console(msg, newline=True, stream='stdout'):
    """Writes the message to the console.

    If the ``newline`` argument is ``True``, a newline character is
    automatically added to the message.

    By default the message is written to the standard output stream.
    Using the standard error stream is possibly by giving the ``stream``
    argument value ``'stderr'``. This is a new feature in RF 2.8.2.
    """
    librarylogger.console(msg, newline, stream)



'''
